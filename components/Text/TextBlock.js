import React from 'react';
import { Text } from 'react-vr';

export default TextBlock = (props) => {
    return (
        <Text style={props.style} >
            {props.text}
        </Text>
    );
}