const styles = {
  backgroundColor: 'transparent',
  color: 'white',
  fontSize: 0.8,
  fontWeight: '400',
  textAlign: 'center',
  textAlignVertical: 'center',

}

const buttonStyle = {
  width: 0.8,
  height: 0.8
}

const returnStyle = (translate, ...rotation) => {
  return Object.assign({}, styles, { transform: [{ translate }, ...rotation] });
}

export const front = returnStyle([0, 0, -7]);
export const back = returnStyle([0, 1.2, 5], { rotateY: 180 });
export const right = returnStyle([5, 2.2, 0], { rotateY: 90 }, { rotateX: 180 }, { rotateZ: 180 });
export const left = returnStyle([-6, 3.2, 0], { rotateY: 90 });
export const up = Object.assign({}, buttonStyle, { transform: [{ translate: [0, 12, 0] }, { rotateX: 90 }] });
export const down = returnStyle([-1, -1, 0], { rotateX: 90 }, { rotateZ: 180 }, { rotateY: 180 });