# Immersive Gallery

Immersive gallery is a fork of the react-vr project, with a simple button that changes the 360 picture randomly - to any one of the images found under `static_assets/imgs`.
This project can, and is encouraged to be used as a starter project. Please also see ReactVR documentation Getting Started section which hosts the base project for this one.  

## Run the project
* `git clone git@gitlab.com:thayut/immersive-gallery.git`  
* `cd immersive-gallery`  
* `npm install`  
* `npm start`

This will start the project at `localhost:8081/vr`  
If you wish to have hot reloading while you are writing code, simply add: `localhost:8081/vr/?hotreload`

## Static assets
The static_assets directory holds all the images. Do note the `images.json` file which hosts the images names and description, which is `import`ed by `index.vr.js`.

## StyleGenerator.js
Under `components/Style` you will find a `StyleGenerator.js` file which holds the style and position for the text blocks displyed over the images.
This example project has preconfigured styles to match top, bottom, left, up, down, right and left facing text blocks.

Checkout the ReactVR documentation:  
https://facebook.github.io/react-vr/