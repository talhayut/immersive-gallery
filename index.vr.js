import React from 'react';
import { images } from './static_assets/images.json';
import { up, down, left, right, back, front } from './components/Style/StyleGenerator';
import {
  AppRegistry,
  asset,
  Pano,
  Text,
  View,
  Image,
  VrButton
} from 'react-vr';

import TextBlock from './components/Text/TextBlock';

export default class ImmersiveGallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = { source: 'waterfall.jpg', name: 'Waterfall' };
  }

  reRenderImage() {
    const sources = images;
    let randomItem = sources[Math.floor(Math.random() * sources.length)];
    this.setState({
      source: randomItem.src,
      name: randomItem.name
    });
  }

  render() {
    return (
      <View>
        <Pano style={{ position: 'absolute' }} source={asset(`imgs/${this.state.source}`)} />
        <TextBlock text={this.state.name} style={front} />
        <TextBlock text={'Back'} style={back} />
        <TextBlock text={'Right'} style={right} />
        <TextBlock text={'Left'} style={left} />
        <VrButton onClick={() => this.reRenderImage()} >
          <Image source={require('./static_assets/imgs/replay.png')} style={up} />
        </VrButton>
        <TextBlock text={'Down'} style={down} />
      </View>

    );
  }
};

AppRegistry.registerComponent('ImmersiveGallery', () => ImmersiveGallery);
